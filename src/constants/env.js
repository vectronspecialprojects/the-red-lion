import {Platform} from 'react-native'

const buildEvn = {
  production: 'production',
  staging: 'staging',
}

const vars = {
  googleApiKey: 'AIzaSyAl60Q1FOsJeZ4RGaF5KCp_Kcf9sGM6p3A',
  oneSignalApiKey: 'b2c42b27-7757-45d9-b436-9bf4d37a1cce',
  buildEvn: buildEvn.production,
}

export const codePushKey = Platform.select({
  ios: {
    staging: 'HmxqEG4jYXXVUAbqNOaXNuj8ip_WOLCLj0w-3',
    production: '_BPGeaB53wiRuUwAcegR8EZVHaxrsR7cFPQbK',
  },
  android: {
    staging: 'yyYR9XD289lxV1YeaP7TPB8BZAaFOrpVrWpyo',
    production: 'jwOQalmISjABLXUzMxuP0nZuZ8P9ZLl53Wy79',
  },
})

export const venueName = 'The Red Lion'
export const venueWebsite = 'https://www.theredlion.com.au/'

//startup page setup
export const isShowLogo = true

//sign up page setup
export const isMultiVenue = false
export const accountMatch = true

export const isGaming = false //need to be true to allow gamingOdyssey or gamingIGT
export const gamingOdyssey = false
export const gamingIGT = false

export const showTierSignup = false //need to be true to allow isCustomField and showMultipleExtended
export const isCustomField = false //need to be true to allow showMultipleExtended
export const showMultipleExtended = false

//home page setup
export const isShowStarBar = false
export const showTierBelowName = false
export const showPointsBelowName = true
export const isShowPreferredVenue = false
export const showTier = true

export const isGroupFilter = false // must be multi venue and has venue tags
export default vars

import React from 'react'
import {View, StyleSheet, Text, Image} from 'react-native'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import Images from '../Themes/Images'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import {SafeAreaView} from 'react-native-safe-area-context';

function MaintenanceScreen() {
  return (
    <View style={styles.container}>
      <SafeAreaView style={styles.logoContainer}>
        <Image style={styles.logo} source={Images.logo} />
      </SafeAreaView>
      <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
        <Image source={Images.maintenance} style={styles.image} />
        <Text style={styles.content}>We are currently undergoing maintenance.</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.defaultBackground,
  },
  content: {
    fontFamily: Fonts.openSansBold,
    color: Colors.white,
    marginTop: responsiveHeight(50),
  },
  image: {
    width: responsiveWidth(200),
    height: responsiveHeight(200),
    tintColor: Colors.white,
  },
  logo: {
    width: responsiveWidth(100),
    height: responsiveHeight(50),
    resizeMode: 'contain',
  },
  logoContainer: {
    alignItems: 'center',
  },
})

export default MaintenanceScreen

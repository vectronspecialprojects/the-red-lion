import React, {useMemo, useEffect} from 'react'
import {View, ActivityIndicator, Text} from 'react-native'
import {useSelector} from 'react-redux'
import {WebView} from 'react-native-webview'

import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const WebviewScreen = (props) => {
  console.log(props)
  const curentPreferredParams = useSelector(
    (state) => state.infoServices.profile?.member?.current_preferred_venue_full,
  )

  const uri = useMemo(() => {
    if (props?.route?.params?.params?.page_layout === 'special_view') {
      if (props?.route?.params?.params?.special_page?.substr(0, 4) === 'link') {
        if (props?.route?.params?.params?.special_page === 'link1') return curentPreferredParams?.link1
        if (props?.route?.params?.params?.special_page === 'link2') return curentPreferredParams?.link2
        if (props?.route?.params?.params?.special_page === 'link3') return curentPreferredParams?.link3
      }
      if (props?.route?.params?.params?.special_page === 'website')
        return props?.route?.params?.params?.special_link
    }
    if (!!props?.route?.params?.params?.appUri) return props?.route?.params?.params?.appUri
  }, [curentPreferredParams, props])

  useEffect(() => {
    if (!uri) {
      Alert.alert(localize('webView.titleAlert'), localize('webView.message'), [{text: localize('okay')}])
    }
  }, [uri])

  return (
    <View style={Styles.screen}>
      <View style={{flex: 1}}>
        <WebView
          startInLoadingState={true}
          source={{uri: uri}}
          renderLoading={() => (
            <View
              style={{
                backgroundColor: Colors.defaultBackground,
                position: 'absolute',
                top: '50%',
                left: '50%',
                right: '50%',
              }}>
              <ActivityIndicator size="large" />
            </View>
          )}
        />
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

export default WebviewScreen

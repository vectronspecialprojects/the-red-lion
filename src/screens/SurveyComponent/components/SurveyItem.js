import React from 'react'
import {View, StyleSheet, Text} from 'react-native'
import Colors from '../../../Themes/Colors'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../../Themes/Metrics'
import Fonts from '../../../Themes/Fonts'
import {TouchableCmp} from '../../../components/UtilityFunctions'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

function SurveyItem({data, onPress}) {
  return (
    <TouchableCmp onPress={onPress}>
      <View style={styles.container}>
        <FontAwesome name="question-circle-o" size={24} color={Colors.survey.icon} />
        <View style={{marginLeft: responsiveWidth(15), flex: 1}}>
          <Text style={styles.title}>{data?.title}</Text>
          {!!data?.point_reward && data?.reward_type === 'point' && (
            <Text style={styles.content}>Get {data?.point_reward} point as a reward</Text>
          )}
          {!!data?.voucher_setups?.name?.length && data?.reward_type !== 'point' && (
            <Text style={styles.content}>Get {data?.voucher_setups?.name} voucher as a reward</Text>
          )}
        </View>
        <MaterialIcons name="chevron-right" size={24} color={Colors.survey.title} />
      </View>
    </TouchableCmp>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: Colors.survey.background,
    marginHorizontal: responsiveWidth(10),
    marginTop: responsiveHeight(17),
    padding: responsiveWidth(15),
    borderRadius: responsiveHeight(10),
    alignItems: 'center',
  },
  title: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSansBold,
    color: Colors.survey.title,
  },
  content: {
    fontSize: responsiveFont(14),
    fontFamily: Fonts.openSans,
    color: Colors.survey.description,
  },
})

export default SurveyItem

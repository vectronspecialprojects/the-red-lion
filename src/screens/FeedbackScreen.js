import React, {useEffect, useState, useCallback} from 'react'
import {View, StyleSheet, Text, TextInput, ScrollView} from 'react-native'
import {useDispatch} from 'react-redux'
import * as infoServicesActions from '../store/actions/infoServices'
import {setGlobalIndicatorVisibility} from '../store/actions/appServices'
import {AirbnbRating} from 'react-native-ratings'
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox'
import {responsiveHeight, responsiveWidth, responsiveFont} from '../Themes/Metrics'
import SubHeaderBar from './../components/SubHeaderBar'
import ButtonView from './../components/ButtonView'
import Styles from '../Themes/Styles'
import Colors from '../Themes/Colors'
import analytics from '@react-native-firebase/analytics'
import Toast from '../components/Toast'
import HeaderLeftButton from '../components/HeaderLeftButton'
import Alert from '../components/Alert'
import {localize} from '../locale/I18nConfig'

const FeedbackScreen = (props) => {
  const title = props.route.params.params.page_name
  const dispatch = useDispatch()
  const [rating, setRating] = useState(0)
  const [comment, setComment] = useState('')
  const [isChecked, setIsChecked] = useState(false)

  useEffect(() => {
    async function firebase() {
      await analytics().logScreenView({screen_name: 'Feedbacks'})
    }
    firebase()
  }, [])

  const submitHandler = useCallback(async () => {
    dispatch(setGlobalIndicatorVisibility(true))
    try {
      const rest = await dispatch(infoServicesActions.submitFeedback(rating, comment, isChecked))
      Toast.success(rest)
      props.navigation.goBack()
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    }
    dispatch(setGlobalIndicatorVisibility(false))
    setRating(0)
    setComment('')
    setIsChecked(false)
  }, [rating, comment, isChecked])

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={title} />
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={{paddingHorizontal: responsiveWidth(20)}}>
          <View
            style={{
              ...Styles.centerContainer,
              ...{height: responsiveHeight(56), marginTop: responsiveHeight(20)},
            }}>
            <Text style={[Styles.xSmallNormalText, {color: Colors.feedback.description}]}>
              {localize('feedback.description')}
            </Text>
          </View>

          <AirbnbRating
            count={5}
            defaultRating={0}
            size={responsiveFont(28)}
            showRating={false}
            selectedColor={Colors.feedback.star}
            onFinishRating={(rating) => setRating(rating)}
          />

          <TextInput
            style={styles.comment}
            value={comment}
            onChangeText={(text) => setComment(text)}
            multiline={true}
            underlineColorAndroid="transparent"
            maxLength={200}
          />

          <View style={{...Styles.centerContainer, ...{marginTop: responsiveHeight(30)}}}>
            <CircleCheckBox
              outerColor={Colors.feedback.checkBoxOuter}
              innerColor={Colors.feedback.checkBoxInner}
              checked={isChecked}
              onToggle={(checked) => setIsChecked(checked)}
              labelPosition={LABEL_POSITION.RIGHT}
              label={localize('feedback.plsContact')}
              styleLabel={{...Styles.xSmallNormalText, ...{textTransform: 'none'}}}
            />
          </View>

          <View style={{marginTop: responsiveHeight(100)}}>
            <ButtonView
              title={localize('feedback.submit')}
              disabled={rating === 0 && isChecked === false && comment.length === 0 ? true : false}
              onPress={submitHandler}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  comment: {
    marginTop: responsiveHeight(40),
    paddingHorizontal: responsiveWidth(20),
    paddingVertical: responsiveWidth(5),
    backgroundColor: Colors.feedback.background,
    color: Colors.feedback.textInput,
    height: responsiveHeight(164),
    width: '100%',
    borderRadius: 15,
    alignSelf: 'center',
    textAlignVertical: 'top',
    lineHeight: responsiveHeight(23),
  },
})

export default FeedbackScreen

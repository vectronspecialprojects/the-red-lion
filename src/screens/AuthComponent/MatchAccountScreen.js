import React, {useEffect, useState, useReducer, useCallback} from 'react'
import {View, StyleSheet} from 'react-native'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import RouteKey from '../../navigation/RouteKey'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import SubHeaderBar from '../../components/SubHeaderBar'
import ButtonView from '../../components/ButtonView'
import TermAndCondition from '../../components/TermAndCondition'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import Toast from '../../components/Toast'
import {accountSearch, getMatchInstruction} from '../../utilities/ApiManage'
import {onLinkPress} from '../../components/UtilityFunctions'
import Html from '../../components/Html'
import Input from '../../components/Input'
import {useDispatch} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {localize} from '../../locale/I18nConfig'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

function MatchAccountScreen({navigation}) {
  const [instruction, setInstruction] = useState('')
  const dispatch = useDispatch()

  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      accountId: '',
      accountNumber: '',
    },
    inputValidities: {
      accountId: false,
      accountNumber: false,
    },
    formIsValid: false,
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier,
      })
    },
    [dispatchFormState],
  )

  useEffect(() => {
    getMatchInstruction().then((res) => {
      if (res.ok) {
        setInstruction(res?.get_started)
      }
    })
  }, [])

  async function handleMatchAccount() {
    try {
      dispatch(setGlobalIndicatorVisibility(true))
      const res = await accountSearch(formState.inputValues.accountId, formState.inputValues.accountNumber)
      console.log('matchaccount', res)
      if (!res.ok) throw new Error(res.message)
      navigation.navigate(RouteKey.MatchAccountInfo, {
        data: res.data?.AccountFull,
      })
    } catch (e) {
      Toast.info(e?.message)
      console.log(e?.message)
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title="Match My Account" />
      <View style={styles.container}>
        <Html
          html={instruction}
          textAlign={'center'}
          color={Colors.whatonDetails.description}
          onLinkPress={onLinkPress}
        />
        <Input
          id="accountId"
          label={localize('matchAccount.accountId')}
          placeholder={localize('matchAccount.accountId')}
          keyboardType="default"
          required
          minLength={2}
          maxLength={15}
          autoCapitalize="none"
          errorText={`${localize('matchAccount.plsEnter')} ${localize('matchAccount.accountId')}`}
          onInputChange={inputChangeHandler}
          initialValue=""
          placeholderTextColor={Colors.matchAccountScreen.placeholderTextColor}
          style={{
            backgroundColor: Colors.matchAccountScreen.fieldBackground,
            color: Colors.matchAccountScreen.fieldText,
          }}
        />
        <Input
          id="accountNumber"
          label={localize('matchAccount.accountNumber')}
          placeholder={localize('matchAccount.accountNumber')}
          keyboardType="default"
          required
          minLength={2}
          maxLength={15}
          autoCapitalize="none"
          errorText={`${localize('matchAccount.plsEnter')} ${localize('matchAccount.accountNumber')}`}
          onInputChange={inputChangeHandler}
          initialValue=""
          placeholderTextColor={Colors.matchAccountScreen.placeholderTextColor}
          style={{
            backgroundColor: Colors.matchAccountScreen.fieldBackground,
            color: Colors.matchAccountScreen.fieldText,
          }}
        />
        <View style={{flex: 1}} />
        <ButtonView
          title={localize('matchAccount.continue')}
          disabled={!formState.formIsValid}
          style={[
            styles.buttonBorder,
            {
              backgroundColor: Colors.matchAccountScreen.backgroundFirstButton,
              borderColor: Colors.matchAccountScreen.boderColorFirstButton,
            },
          ]}
          titleStyle={{color: Colors.matchAccountScreen.textFirstButton}}
          onPress={() => handleMatchAccount()}
        />
        <ButtonView
          title={localize('matchAccount.cancel')}
          style={[
            styles.buttonBorder,
            {
              backgroundColor: Colors.matchAccountScreen.backgroundSecondButton,
              borderColor: Colors.matchAccountScreen.boderColorSecondButton,
            },
          ]}
          titleStyle={{color: Colors.matchAccountScreen.textSecondButton}}
          onPress={() => {
            navigation.pop()
          }}
        />
        <TermAndCondition />
      </View>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: responsiveWidth(24),
  },
  text: {
    fontSize: responsiveFont(15),
    color: Colors.white,
    textAlign: 'center',
    marginBottom: responsiveHeight(20),
  },
  buttonBorder: {
    borderWidth: 2,
    backgroundColor: Colors.defaultBackground,
    marginBottom: responsiveHeight(8),
  },
})

export default MatchAccountScreen

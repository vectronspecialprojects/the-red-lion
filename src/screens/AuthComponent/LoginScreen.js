import React, {useReducer, useCallback} from 'react'
import {View, Text, StyleSheet, ScrollView} from 'react-native'
import {useDispatch} from 'react-redux'
import TermAndCondition from '../../components/TermAndCondition'
import SubHeaderBar from '../../components/SubHeaderBar'
import Input from '../../components/Input'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import ButtonView from '../../components/ButtonView'
import RouteKey from '../../navigation/RouteKey'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import {login} from '../../store/actions/authServices'
import Fonts from '../../Themes/Fonts'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig'

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.inputValues,
      [action.input]: action.value,
    }
    const updatedValidities = {
      ...state.inputValidities,
      [action.input]: action.isValid,
    }
    let updatedFormIsValid = true
    for (const key in updatedValidities) {
      updatedFormIsValid = updatedFormIsValid && updatedValidities[key]
    }
    return {
      formIsValid: updatedFormIsValid,
      inputValidities: updatedValidities,
      inputValues: updatedValues,
    }
  }
  return state
}

const LoginScreen = (props) => {
  const dispatch = useDispatch()
  const [formState, dispatchFormState] = useReducer(formReducer, {
    inputValues: {
      email: '',
      password: '',
    },
    inputValidities: {
      email: false,
      password: false,
    },
    formIsValid: false,
  })

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier,
      })
    },
    [dispatchFormState],
  )

  const loginHandler = async () => {
    dispatch(setGlobalIndicatorVisibility(true))
    try {
      await dispatch(login(formState.inputValues.email, formState.inputValues.password))
    } catch (err) {
      Alert.alert(localize('signIn.titleErrSignIn'), err.message, [{text: localize('okay')}])
    } finally {
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar title={localize('signIn.signIn')} />
      <ScrollView keyboardShouldPersistTaps={'handled'}>
        <View style={{flex: 1, paddingHorizontal: responsiveWidth(20)}}>
          <View>
            <Input
              id="email"
              label={localize('signIn.email')}
              placeholder={localize('signIn.email')}
              keyboardType="email-address"
              required
              email
              autoCapitalize="none"
              errorText={`${localize('signIn.plsEnter')} ${localize('signIn.email')}`}
              onInputChange={inputChangeHandler}
              initialValue=""
              placeholderTextColor={Colors.signIn.placeholderTextColor}
              style={{
                backgroundColor: Colors.signIn.fieldBackground,
                color: Colors.signIn.fieldText,
              }}
            />
            <Input
              id="password"
              label={localize('signIn.password')}
              placeholder={localize('signIn.password')}
              keyboardType="default"
              secureTextEntry
              required
              autoCapitalize="none"
              errorText={localize('signIn.errPassword')}
              onInputChange={inputChangeHandler}
              initialValue=""
              placeholderTextColor={Colors.signIn.placeholderTextColor}
              style={{
                backgroundColor: Colors.signIn.fieldBackground,
                color: Colors.signIn.fieldText,
              }}
            />
          </View>
          <View
            style={{
              ...Styles.centerContainer,
              ...{marginTop: responsiveHeight(30)},
            }}>
            <Text
              style={styles.textButton}
              onPress={() => {
                props.navigation.navigate(RouteKey.ForgotPasswordScreen)
              }}>
              {localize('signIn.forgotPassword')}
            </Text>
          </View>
          <View style={styles.buttonContainer}>
            <ButtonView
              title={localize('signIn.signIn')}
              disabled={!formState.formIsValid}
              style={[
                styles.buttonBorder,
                {
                  backgroundColor: Colors.signIn.backgroundSigninButton,
                  borderColor: Colors.signIn.borderSigninButton,
                },
              ]}
              titleStyle={{color: Colors.signIn.textSigninButton}}
              onPress={loginHandler}
            />
          </View>
          <TermAndCondition />
        </View>
      </ScrollView>
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    marginVertical: responsiveHeight(50),
  },
  textButton: {
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(8),
    color: Colors.signIn.textForgotPassword,
    textTransform: 'capitalize',
  },
  termContainer: {
    alignItems: 'center',
    paddingHorizontal: responsiveWidth(20),
  },
  buttonBorder: {
    borderWidth: 2,
    marginBottom: responsiveHeight(8),
  },
})

export default LoginScreen

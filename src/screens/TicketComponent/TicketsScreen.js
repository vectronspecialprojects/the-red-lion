import React, {useEffect, useState, useCallback, useMemo} from 'react'
import {View, StyleSheet, FlatList, RefreshControl, Text} from 'react-native'
import SubHeaderBar from '../../components/SubHeaderBar'
import Styles from '../../Themes/Styles'
import Colors from '../../Themes/Colors'
import * as infoServicesActions from '../../store/actions/infoServices'
import {useDispatch, useSelector} from 'react-redux'
import {setGlobalIndicatorVisibility} from '../../store/actions/appServices'
import TicketItem from './components/TicketItem'
import Filter from '../../components/Filter'
import {responsiveHeight, responsiveWidth} from '../../Themes/Metrics'
import RouteKey from '../../navigation/RouteKey'
import HeaderLeftButton from '../../components/HeaderLeftButton'
import Alert from '../../components/Alert'
import {localize} from '../../locale/I18nConfig';

const TicketsScreen = ({route, navigation}) => {
  const title = route.params.params?.page_name
  const tickets = useSelector((state) => state.infoServices.tickets)
  const [isFilter, setIsFilter] = useState(false)
  const [selectedId, setSelectedId] = useState(useSelector((state) => state.infoServices.preferredVenueId))
  const [isRefreshing, setIsRefreshing] = useState(false)
  const dispatch = useDispatch()

  const loadContent = useCallback(async () => {
    try {
      await dispatch(infoServicesActions.fetchTickets())
    } catch (err) {
      Alert.alert(localize('somethingWentWrong'), err.message, [{text: localize('okay')}])
    } finally {
      setIsRefreshing(false)
      dispatch(setGlobalIndicatorVisibility(false))
    }
  }, [dispatch])

  useEffect(() => {
    dispatch(setGlobalIndicatorVisibility(true))
    loadContent()
  }, [dispatch, loadContent])

  const filterHandler = (venue) => {
    setSelectedId(venue?.id)
    setIsFilter(false)
  }

  const showData = useMemo(() => {
    if (tickets === undefined) return []
    if (tickets !== undefined) {
      if (selectedId === 0) return tickets
      return tickets.filter(
        (ticket) => ticket.order_detail?.venue?.id === 0 || ticket.order_detail?.venue?.id === selectedId,
      )
    }
  }, [tickets, selectedId])

  function renderItem({item, index}) {
    return (
      <TicketItem
        data={item}
        onPress={() =>
          navigation.navigate(RouteKey.TicketDetailScreen, {
            ticketDetail: item,
          })
        }
      />
    )
  }

  return (
    <View style={Styles.screen}>
      <SubHeaderBar
        title={title}
        filterBtn={true}
        onFilterPress={() => {
          setIsFilter(true)
        }}
      />
      <Filter
        isVisible={isFilter}
        backScreenOnPress={() => {
          setIsFilter(false)
        }}
        selectedId={selectedId}
        onFilterPress={filterHandler}
      />
      <FlatList
        data={showData}
        style={{marginHorizontal: responsiveWidth(25), marginBottom: responsiveHeight(10)}}
        renderItem={renderItem}
        numColumns={2}
        ListEmptyComponent={
          <Text style={Styles.flatlistNoItems}>No Ticket found, please check again later.</Text>
        }
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => {
              setIsRefreshing(true)
              loadContent()
            }}
            tintColor={Colors.defaultRefreshSpinner}
            titleColor={Colors.defaultRefreshSpinner}
            title={localize('pullToRefresh')}
          />
        }
      />
    </View>
  )
}

export const screenOptions = (navData) => {
  return {
    headerLeft: () => <HeaderLeftButton navData={navData} />,
  }
}

const styles = StyleSheet.create({})

export default TicketsScreen

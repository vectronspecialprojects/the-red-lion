import {updateObject} from '../../utilities/utils'
import {REHYDRATE} from 'redux-persist'
import {ActionSheetIOS} from 'react-native'
import * as act from '../actions/actionCreator'

const initialState = {
  showGlobalIndicator: false,
  galleries: [],
  homeMenus: [],
  drawerMenus: [],
  drawerFrontMenus: [],
  tabMenus: [],
  isAppSetup: true,
  isMaintenance: false,
  appState: 'SplashScreen',
  internetState: true,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case act.GET_APPSETTINGS:
      return updateObject(state, {
        galleries: action.galleries,
        homeMenus: action.homeMenus,
        drawerMenus: action.drawerMenus,
        drawerFrontMenus: action.drawerFrontMenus,
        tabMenus: action.tabMenus,
        isAppSetup: action.isAppSetup
      })
    case act.SET_GLOBAL_INDICATOR_VISIBILITY:
      return updateObject(state, {showGlobalIndicator: action.visible})
    case act.SET_APP_MAINTENANCE:
      return updateObject(state, {isMaintenance: action.payload})
    case act.SET_APP_STATE:
      return updateObject(state, {appState: action.payload})
    case act.AUTHENTICATE:
      return updateObject(state, {appState: 'Main'})
    case REHYDRATE:
      return updateObject(state, {
        ...(action.payload?.app || {}),
        appState: 'SplashScreen',
        showGlobalIndicator: false,
      })
    case act.SET_INTERNET_STATE:
      return updateObject(state, {internetState: action.payload})
    case act.LOGOUT:
      return updateObject(state, {appState: 'Auth'})
    default:
      return state
  }
}

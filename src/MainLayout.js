import React, {useEffect} from 'react'
import AppNavigation from './navigation/AppNavigator'
import {Linking, StatusBar, StyleSheet, View} from 'react-native'
import IndicatorDialog from './components/IndicatorDialog'
import {connect, useDispatch, useSelector} from 'react-redux'
import Toast from './components/Toast'
import NetInfo from '@react-native-community/netinfo'
import {setGlobalIndicatorVisibility, setInternetState} from './store/actions/appServices'
import {oneSignalHandlers, setUpOneSignal, sendTags} from './utilities/OneSignal'
import Alert from './components/Alert'
import {checkTokenValid, verifyIdToken} from './store/actions/authServices'
import {navigate} from './navigation/NavigationService'
import RouteKey from './navigation/RouteKey'

function MainLayout({showGlobalIndicator}) {
  const dispatch = useDispatch();
  const user_credentials = useSelector(
    state => state.authServices.user_credentials,
  );
  const profile = useSelector(state => state.infoServices.profile);
  const appState = useSelector(state => state.app.appState);

  useEffect(() => {
    handleCheckNetwork();
    setUpOneSignal();
    oneSignalHandlers();
  }, []);

  useEffect(() => {
    if (appState === 'Main') {
      handleDeepLink();
      sendTags({
        email: profile?.email,
        tier_name: profile?.member?.member_tier?.tier?.name,
        tier_id: profile?.member?.member_tier?.tier?.id,
      });
    }
  }, [appState]);

  function handleCheckNetwork() {
    NetInfo.addEventListener(state => {
      if (
        (state.type === 'wifi' || state.type === 'cellular') &&
        state.isConnected
      ) {
        dispatch(setInternetState(true));
      } else {
        dispatch(setInternetState(false));
      }
    });
  }

  function handleDeepLink() {
    Linking.getInitialURL().then(res => {
      //This function only work when disable debug mode.
      if (res) {
        handleValidateToken(res);
      }
      console.log('getInitialURL', res);
    });
    Linking.addEventListener('url', res => {
      if (res?.url) {
        handleValidateToken(res?.url);
      }
      console.log('addEventListener', res.url);
    });
  }

  async function handleValidateToken(link) {
    try {
      dispatch(
        checkTokenValid(async (value) => {
          if (value?.id_token) {
            dispatch(setGlobalIndicatorVisibility(true))
            await dispatch(verifyIdToken())
            setTimeout(() => {
              navigate(RouteKey.YourorderScreen, {
                params: {
                  qrcodeUri: link,
                },
              })
            }, 0)
          }
        }),
      )
    } catch (e) {
      console.log(e);
    } finally {
      dispatch(setGlobalIndicatorVisibility(false));
    }
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <AppNavigation />
      {showGlobalIndicator && <IndicatorDialog />}
      <Toast />
      <Alert />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default connect(
  state => ({
    showGlobalIndicator: state.app.showGlobalIndicator,
  }),
  dispatch => ({}),
)(MainLayout);

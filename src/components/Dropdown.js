import React, {useState, useEffect} from 'react'
import {StyleSheet, View, Text} from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Fonts from '../Themes/Fonts'
import Colors from '../Themes/Colors'

const Dropdown = (props) => {
  const {style, placeholder} = props
  const [isValid, setIsValid] = useState(false)
  const [value, setValue] = useState(null)
  const {onInputChange, id, items} = props
  const valueChangeHandler = (value) => {
    let isValid = true
    if (props.required && value === null) isValid = false
    setIsValid(isValid)
    setValue(value)
  }

  useEffect(() => {
    if (items?.length === 1) {
      valueChangeHandler(items[0].value)
    }
  }, [props.items])

  useEffect(() => {
    onInputChange(id, value, isValid)
  }, [onInputChange, id, value, isValid])

  return (
    <View style={styles.formControl}>
      <Text style={styles.label}>{props?.label}</Text>
      <RNPickerSelect
        placeholder={{
          label: placeholder,
          value: null,
        }}
        style={{...pickerSelectStyles, ...style}}
        onValueChange={valueChangeHandler}
        items={props.items}
        value={value}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  formControl: {
    width: '100%',
  },
  label: {
    fontFamily: Fonts.openSansBold,
    marginVertical: responsiveHeight(4),
    color: Colors.defaultTextColor,
  },
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    paddingHorizontal: responsiveWidth(20),
    paddingVertical: responsiveHeight(5),
    backgroundColor: Colors.signUp.fieldBackground,
    color: Colors.signUp.fieldText,
    fontFamily: Fonts.openSans,
    fontSize: 14,
    height: responsiveHeight(48),
    borderRadius: 15,
  },
  inputAndroid: {
    paddingHorizontal: responsiveWidth(10),
    paddingVertical: responsiveHeight(5),
    backgroundColor: Colors.signUp.fieldBackground,
    color: Colors.signUp.fieldText,
    fontFamily: Fonts.openSans,
    fontSize: 14,
    height: responsiveHeight(48),
    borderRadius: 15,
  },
})

export default Dropdown

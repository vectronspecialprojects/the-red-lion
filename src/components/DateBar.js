import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Styles from '../Themes/Styles'

const DateBar = (props) => {
  return (
    <View style={{...styles.bar, ...props.style}}>
      {props.days?.map((day) => (
        <View key={day.order} style={[Styles.dayFrame, {borderColor: props.color}]}>
          <Text style={{...Styles.xSmallUpText, ...{color: props.color}}}>{day.name.substr(0, 3)}</Text>
        </View>
      ))}
    </View>
  )
}

const styles = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: responsiveHeight(10),
  },
})

export default DateBar

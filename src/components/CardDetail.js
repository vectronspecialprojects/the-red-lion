import React from 'react'
import {View, Text, StyleSheet, Share} from 'react-native'
import CardHeader from './CardHeader'
import Html from '../components/Html'
import CartBar from './CartBar'
import {onLinkPress} from './UtilityFunctions'
import {responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import Colors from '../Themes/Colors'
import Styles from '../Themes/Styles'
import DateBar from './DateBar'
import {navigate} from '../navigation/NavigationService'
import RouteKey from '../navigation/RouteKey'
import Toast from './Toast'
import {venueWebsite} from '../constants/env'

const CardDetail = (props) => {
  function handleCartPress() {
    if (props.handleCartPress) {
      props.handleCartPress()
    } else {
      navigate(RouteKey.ProductListScreen, {
        products: props.products,
        listingId: props.data?.id,
        type: props.data?.type?.id,
      })
    }
  }

  const handleShare = async () => {
    try {
      await Share.share(
        {
          title: props.title,
          message: `${props.title}\n${props.data?.desc_short}`,
          url:
            JSON.parse(props?.data?.venue?.social_links).filter((item) => item?.platform === 'website')[0]
              ?.url || venueWebsite,
        },
        {
          dialogTitle: 'Pick an App',
        },
      )
    } catch (error) {
      Toast.info(error.message)
    }
  }

  return (
    <View>
      {!!props?.title && (
        <CardHeader
          title={props.title}
          style={{marginVertical: responsiveHeight(10)}}
          titleStyle={props.titleStyle}
        />
      )}
      {!!props?.venueName && <Text style={[Styles.xSmallUpText, styles.venueName]}>{props.venueName}</Text>}

      {!!props?.html && (
        <View style={{paddingHorizontal: responsiveWidth(20)}}>
          <Html
            html={props.html}
            color={Colors.whatonDetails.description}
            onLinkPress={onLinkPress}
            textAlign={'center'}
          />
        </View>
      )}
      {!!props?.startTime && !!props?.endTime && (
        <Text
          style={{
            ...Styles.smallNormalText,
            ...{
              color: Colors.whatonDetails.description,
              alignSelf: 'center',
              marginTop: responsiveHeight(10),
            },
          }}>
          START: {props?.startTime} END: {props?.endTime}
        </Text>
      )}
      {!!props?.days && <DateBar days={props.days} color={Colors.whatonDetails.dateColor} />}
      <CartBar
        style={{marginTop: responsiveHeight(30)}}
        favorite={props.favorite}
        onFavoritePress={props.onFavoritePress}
        onChatPress={props.onChatPress}
        onCartPress={handleCartPress}
        onSharePress={handleShare}
        allowBooking={props.allowBooking}
        allowChat={props.allowChat}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  venueName: {
    marginBottom: responsiveWidth(10),
    alignSelf: 'center',
  },
})

export default CardDetail

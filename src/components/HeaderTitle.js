import {Text, StyleSheet, View} from 'react-native';
import React from 'react';
import {
  responsiveFont,
  responsiveHeight,
  responsiveWidth,
} from '../Themes/Metrics';
import Colors from '../Themes/Colors';
import Fonts from '../Themes/Fonts';

export default function({title, subTitle, style, titleStyle, subStyle}) {
  return (
    <View style={style}>
      <Text style={[styles.title, titleStyle]}>{title}</Text>
      {subTitle && <Text style={[styles.subTitle, subStyle]}>{subTitle}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: responsiveFont(34),
    color: Colors.white,
    marginHorizontal: responsiveWidth(20),
  },
  subTitle: {
    fontSize: responsiveFont(14),
    marginTop: responsiveHeight(10),
    marginHorizontal: responsiveWidth(20),
    color: Colors.gray,
  },
});

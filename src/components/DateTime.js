import {hitSlop, isIOS, responsiveFont, responsiveHeight, responsiveWidth} from '../Themes/Metrics'
import React, {useCallback, useState} from 'react'
import {View, StyleSheet, TouchableNativeFeedback, Text} from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker'
import moment from 'moment'
import Colors from '../Themes/Colors'
import {TouchableCmp} from './UtilityFunctions'
import {isDarkMode} from '../utilities/utils'
import DateTimePickerModal from 'react-native-modal-datetime-picker'

function DateTime({
  value,
  onChange,
  mode = 'date',
  is24Hour = true,
  format = 'YYYY-MM-DD',
  textStyle,
  maxDate,
  minDate,
  pickerStyle,
  containerStyle,
}) {
  const [showDateTime, setShowDateTime] = useState(false)

  function handleConfirmDate(date) {
    setShowDateTime(false)
    onChange?.(date)
  }

  function handleCancelDate() {
    setShowDateTime(false)
  }

  const renderDateTime = useCallback(() => {
    return (
      <View style={[styles.container, containerStyle]}>
        <TouchableCmp onPress={() => setShowDateTime(true)} hitSlop={hitSlop}>
          <Text style={[styles.textStyle, textStyle]}>{value ? moment(value).format(format) : format}</Text>
        </TouchableCmp>
        <DateTimePickerModal
          date={value || new Date()}
          mode={mode}
          is24Hour={is24Hour}
          isDarkModeEnabled={isDarkMode()}
          isVisible={showDateTime}
          onConfirm={handleConfirmDate}
          onCancel={handleCancelDate}
          maximumDate={maxDate || null}
          minimumDate={minDate || null}
        />
      </View>
    )
  }, [value, maxDate, minDate, showDateTime])

  return <View style={[styles.container, containerStyle]}>{renderDateTime()}</View>
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: responsiveWidth(10),
    flex: 1,
    justifyContent: 'center',
  },
  textStyle: {
    fontSize: responsiveFont(15),
    color: Colors.defaultTextColor,
  },
  dateTimerPicker: {
    width: responsiveWidth(130),
    height: responsiveHeight(40),
    marginLeft: responsiveWidth(12),
  },
})

export default DateTime

import {StyleSheet, Text, View} from 'react-native'
import React from 'react'
import Colors from '../Themes/Colors'
import Fonts from '../Themes/Fonts'
import {TouchableCmp} from './UtilityFunctions';
import {responsiveFont, responsiveHeight, responsiveWidth, shadow} from '../Themes/Metrics';

export default function ButtonView({
  title,
  desc,
  style,
  onPress,
  disabled,
  hasIcon,
  titleStyle,
  descStyle,
  children,
  wrapper,
  leftIcon,
  isShadow,
}) {
  return (
    <View style={[styles.container, style, isShadow && {...shadow}, disabled && {opacity: 0.5}]}>
      <TouchableCmp style={{flex: 1}} disabled={disabled} onPress={onPress}>
        <View style={styles.textWrapper}>
          {!!leftIcon && leftIcon}
          {title && (
            <Text style={[styles.titleStyle, !hasIcon && {textAlign: 'center'}, titleStyle]}>{title}</Text>
          )}
          {desc && (
            <Text style={[styles.descStyle, !hasIcon && {textAlign: 'center'}, descStyle]}>{desc}</Text>
          )}
          {children}
        </View>
      </TouchableCmp>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    // minHeight: 48,
    height: responsiveHeight(52),
    borderRadius: 12,
    backgroundColor: Colors.dufaultButtonBackground,
    overflow: 'hidden',
  },
  textWrapper: {
    flex: 1,
    height: responsiveHeight(52),
    borderRadius: 12,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  wrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: responsiveWidth(16),
  },
  titleStyle: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(15),
    color: Colors.defaultButtonText,
  },
  descStyle: {
    fontFamily: Fonts.openSansBold,
    fontSize: responsiveFont(10),
    color: Colors.defaultButtonText,
  },
})

import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFont,
} from '../Themes/Metrics';
import * as infoServicesActions from '../store/actions/infoServices';
import {useDispatch} from 'react-redux';
import Styles from '../Themes/Styles';
import Colors from '../Themes/Colors';
import {wait} from './UtilityFunctions';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HtmlTextPopup from './HtmlTextPopup';
import {isMultiVenue} from '../constants/env';
import {isEmptyValues} from '../utilities/utils';

const Notification = props => {
  const [toggle, setToggle] = useState(false);
  const [isPopupVisible, setIsPopupVisible] = useState(false);
  const [showMsg, setShowMsg] = useState({});
  const dispatch = useDispatch();

  const notificationHandle = async id => {
    setIsPopupVisible(false);
    try {
      await dispatch(infoServicesActions.deleteNotification(id));
      if (id !== 0 && !isEmptyValues(showMsg))
        wait(400).then(() =>
          props.navigation.navigate(
            JSON.parse(showMsg.notification.payload).react,
          ),
        );
    } catch (err) {
      console.log(err);
    } finally {
      setShowMsg({});
    }
  };

  return (
    <View
      style={{
        marginBottom: isMultiVenue ? responsiveHeight(5) : 0,
        marginTop: isMultiVenue ? 0 : responsiveHeight(15),
      }}>
      <View>
        <TouchableOpacity style={{flex: 1}} onPress={() => setToggle(!toggle)}>
          <View style={styles.container}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <FontAwesome
                name="bell-o"
                size={responsiveFont(24)}
                color={Colors.notification.iconColor}
              />
              <Text
                style={[
                  Styles.xSmallBoldText,
                  {
                    marginLeft: responsiveWidth(10),
                    color: Colors.notification.textColor,
                  },
                ]}>
                You have got {props.notifications?.length} messages.
              </Text>
            </View>
            <TouchableOpacity onPress={notificationHandle.bind(this, 0)}>
              <Text
                style={[
                  Styles.xSmallUpBoldText,
                  {
                    marginRight: responsiveWidth(10),
                    color: Colors.notification.textColor,
                  },
                ]}>
                clear all
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </View>

      {toggle &&
        props.notifications.map(message => (
          <View key={message.id} style={{marginTop: responsiveHeight(5)}}>
            <TouchableOpacity
              style={{flex: 1}}
              onPress={() => {
                setShowMsg(message);
                setIsPopupVisible(true);
              }}>
              <View style={styles.container}>
                <Text
                  style={[
                    Styles.xSmallBoldText,
                    {
                      marginLeft: responsiveWidth(35),
                      color: Colors.notification.textColor,
                    },
                  ]}>
                  {message.notification.title}
                </Text>
                <TouchableOpacity
                  onPress={notificationHandle.bind(this, message.id)}>
                  <Ionicons
                    name="close-sharp"
                    size={responsiveFont(24)}
                    color={Colors.notification.iconColor}
                  />
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          </View>
        ))}
      <HtmlTextPopup
        isVisible={isPopupVisible}
        header={showMsg.notification?.title}
        text={showMsg.notification?.message}
        onOkPress={notificationHandle.bind(this, showMsg.id)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: Colors.notification.backgroundColor,
    marginHorizontal: 0,
    borderWidth: 1,
    borderColor: Colors.notification.borderColor,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
  },
});

export default Notification;

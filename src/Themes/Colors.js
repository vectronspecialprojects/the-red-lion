export default {
  defaultRefreshSpinner: '#a10e1a',
  defaultBackground: '#202224',
  headerBackground: '#25282a',
  subHeaderBackground: '#25282a',
  defaultFilterIcon: '#a10e1a',
  defaultFilterBackground: '#fff',
  defaultFilterText: '#444',
  headerLeftIcon: '#fff',
  pageTitle: '#a10e1a',
  defaultTextColor: '#ffffff',
  defaultTextInputBackgound: '#25282a',
  defaultTextInputLabelColor: '#ffffff',
  defaultTextInputColor: '#ffffff',
  defaultPlaceholderTextColor: '#fff',
  termAndConditionTextColor: '#a10e1a',
  dufaultButtonBackground: '#a10e1a',
  defaultButtonText: '#fff',
  defaultAlertTextTitle: '#000',
  defaultAlertTextmessage: '#000',
  defaultCartBarBackground: '#25282a',
  btnDone: '#0405cf', // new version

  first: '#25282a',
  second: '#a10e1a',
  cartBarItems: '#ffffff',
  cartBarItemsActive: '#FF3838',
  flatlistNoItems: '#444',
  red: 'red',
  white: '#fff',
  black: '#000',
  opacity: 'rgba(0,0,0,0.45)',
  gray: 'gray',
  notification: {
    borderColor: '#000',
    backgroundColor: '#f0f0f0',
    textColor: '#000',
    iconColor: '#000',
  },
  firstPage: {
    firstButtonBackground: '#a10e1a',
    firstButtonBorder: '#a10e1a', //new version
    firstButtonText: '#fff',
    secondButtonBackground: '#25282a',
    secondButtonBorder: '#25282a', // new version
    secondButtonText: '#fff',
    //match button style
    borderMatchButton: '#a10e1a',
    backgroundMatchButton: '#202224',
    textMatchButton: '#a10e1a',
  },
  home: {
    name: '#fff',
    description: '#fff',
    menuBackground: '#25282a',
    menuIcon: '#fff',
    menuDesc: '#a10e1a',
    menuText: '#fff',
    preferredVenueButton: '#25282a',
    preferredVenueButtonIcon: '#fff',
    tierBackground: '#a10e1a',
    iconStar: '#a10e1a',
    iconBackground: '#fff',
    venueTitle: '#a10e1a',
    venueDesc: '#fff',
    tierBarText: '#fff',
    barcodeGeneratingText: '#ffffff',
    barcodeTopBorder: '#25282a',
    barcodeAreaBackground: '#202224',
  },
  whaton: {
    tabBackgroundActive: '#a10e1a',
    tabBackgroundInactive: 'transparent',
    tabTitleActive: '#fff',
    tabTitleInactive: '#a10e1a',
    titleColor: '#fff',
    description: '#fff',
    dateColor: '#fff',
  },
  whatonDetails: {
    title: '#fff',
    description: '#fff',
    dateColor: '#fff',
  },
  stampcard: {
    cardTitle: '#a10e1a',
    starIcon: '#a10e1a',
  },
  giftCertificate: {
    cardTitle: '#fff',
    cardTitleExpanded: '#a10e1a',
  },
  buyGiftCertificate: {
    descriptionText: '#fff',
    inputText: '#444',
  },
  about: {
    description: '#fff',
  },
  legal: {
    iconArrow: '#fff',
    title: '#fff',
    description: '#fff',
  },
  voucher: {
    title: '#fff',
    date: '#fff',
    description: '#fff',
  },
  location: {
    tagsBackgroundActive: '#a10e1a',
    tagsBackgroundInactive: '#202224',
    tagsTitleActive: '#fff',
    tagsTitleInactive: '#a10e1a',
    title: '#fff',
    address: '#fff',
    description: '#fff',
    openHours: '#a10e1a',
    cardBackground: '#202224',
    tagsBarBackground: '#25282a',
  },
  faq: {
    searchBox: '#25282a',
    title: '#a10e1a',
    description: '#fff',
    background: '#25282a',
  },
  survey: {
    title: '#fff',
    description: '#fff',
    background: '#25282a',
    icon: '#a10e1a',
    questionTitle: '#a10e1a',
    answer: '#fff',
  },
  referFriends: {
    placeHolder: '#444',
    textInput: '#444',
    textInputBackground: '#fff',
  },
  ticket: {
    title: '#fff',
    date: '#fff',
    description: '#fff',
  },
  ourTeam: {
    title: '#fff',
    description: '#fff',
    name: '#a10e1a',
    background: '#25282a',
    buttonBackground: '#25282a',
    buttonTitle: '#a10e1a',
  },
  offer: {
    title: '#fff',
    description: '#fff',
    point: '#a10e1a',
    redeemButtonBackground: '#202224',
    redeemButtonBorder: '#a10e1a',
    redeemButtonText: '#a10e1a',
    cancelButtonBackground: '#202224',
    cancelButtonBorder: '#a10e1a',
    cancelButtonText: '#a10e1a',
    popupBackground: '#202224',
  },
  favorite: {
    title: '#a10e1a',
    description: '#fff',
  },
  history: {
    background: '#25282a',
    text: '#fff',
    filterText: '#a10e1a',
  },
  profile: {
    borderButtonColor: '#a10e1a',
    backgroundButton: '#202224',
    buttonText: '#fff',
    textInput: '#fff',
    icon: '#fff',
  },
  resetPassword: {
    secondButtonBackground: '#fff',
    secondButtonBorder: '#000',
    secondButtonText: '#000',
  },
  changeEmail: {
    secondButtonBackground: '#fff',
    secondButtonBorder: '#000',
    secondButtonText: '#000',
  },
  signUp: {
    fieldBackground: '#25282a',
    fieldText: '#fff',
    backgroundSignupButton: '#a10e1a',
    textSignupButton: '#ffffff',
    placeholderTextColor: '#eee',
  },
  signIn: {
    //forgot password style
    textForgotPassword: '#a10e1a',
    //sign in button style
    fieldText: '#fff',
    backgroundSigninButton: '#a10e1a',
    borderSigninButton: '#a10e1a',
    textSigninButton: '#ffffff',
    placeholderTextColor: '#eee',
    fieldBackground: '#25282a',
  },
  preferredVenue: {
    icon: '#fff',
    title: '#fff',
    background: '#202224',
    venueActive: '#a10e1a',
    venueActiveText: '#000',
  },
  feedback: {
    description: '#fff',
    star: '#a10e1a',
    checkBoxOuter: '#a10e1a',
    checkBoxInner: '#a10e1a',
    background: '#25282a',
    textInput: '#fff',
  },
  drawer: {
    background: '#25282a',
    title: '#fff',
  },
  tabs: {
    tabBackground: '#202224',
    tabLabel: '#ffffff',
    activeTabLabel: '#a10e1a',
  },
  paymentPopup: {
    totalAmount: '#fff',
    buttonBackground: '#000',
    buttonText: '#fff',
  },
  forgotPassword: {
    buttonBackground: '#000',
    buttonBorder: '#000',
    buttonText: '#fff',
    placeholderTextColor: '#fff',
  },
  matchAccountScreen: {
    placeholderTextColor: '#eee',
    fieldBackground: '#25282a',
    fieldText: '#fff',
    backgroundFirstButton: '#a10e1a',
    boderColorFirstButton: '#a10e1a',
    textFirstButton: '#fff',
    backgroundSecondButton: '#25282a',
    boderColorSecondButton: '#25282a',
    textSecondButton: '#fff',
  },
  productItemScreen: {
    cardBackground: '#25282a',
    productName: '#fff',
    price: '#fff',
    buttonBackground: '#a10e1a',
    buttonBorder: '#fff',
    buttonIcon: '#fff',
    quantityText: '#fff',
  },
  buyGiftCertificateScreen: {
    destColor: '#fff',
  },
  // updated code start from here
  gamingScreen: {
    gameItemBackground: '#25282a',
  },
  membershipScreen: {
    cardTitle: '#a10e1a',
  },
}

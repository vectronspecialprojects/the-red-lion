import vars from '../constants/env'
import OneSignal from 'react-native-onesignal'
export function setUpOneSignal() {
  OneSignal.setAppId(vars.oneSignalApiKey)
  OneSignal.setRequiresUserPrivacyConsent(false)
  OneSignal.promptForPushNotificationsWithUserResponse((response) => {
    console.log(response)
  })
}

export function oneSignalHandlers() {
  OneSignal.setNotificationWillShowInForegroundHandler((notifReceivedEvent) => {
    let noti = notifReceivedEvent.getNotification()
    console.log(noti)
  })
  OneSignal.setNotificationOpenedHandler((notification) => {
    console.log('OneSignal: notification opened:', notification)
  })
  OneSignal.addEmailSubscriptionObserver((event) => {
    console.log('OneSignal: email subscription changed: ', event)
  })
  OneSignal.addSubscriptionObserver((event) => {
    console.log('OneSignal: subscription changed:', event)
  })
  OneSignal.addPermissionObserver((event) => {
    console.log('OneSignal: permission changed:', event)
  })
}

export function removeHandlers() {
  OneSignal.clearHandlers()
}

export function sendTags(tags) {
  console.log('onesignalTags', tags)
  OneSignal.sendTags(tags)
}

export function deleteTags() {
  OneSignal.deleteTags(['email', 'tier_name', 'tier_id'])
}
